import Vue from 'vue'
import Router from 'vue-router'

//views
import Scores from '@/views/Scores'
import Rules from '@/views/Rules'
import Equipment from '@/views/Equipment'
import Health from '@/views/Health'


//containers
import Full from '@/containers/Full'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
      {
        path: '/',
        component: Full,
        children: [
          {
            path: '/',
            name: 'home',
            component: Scores
          },
          {
            path: '/scores',
            name: 'scores',
            component: Scores
          },
          {
            path: '/rules',
            name: 'rules',
            component: Rules
          },
          {
            path: '/equipment',
            name: 'equipment',
            component: Equipment
          },
          {
            path: '/health',
            name: 'health',
            component: Health
          }
        ]
     }
   ]
})
