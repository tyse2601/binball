const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  DATABASE_URI: `"${process.env.DATABASE_URI}"`,
  BINBALL_URI: `"${process.env.BINBALL_URI}"`
})
